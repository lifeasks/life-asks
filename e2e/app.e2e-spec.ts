import { LifeAsksPage } from './app.po';

describe('life-asks App', () => {
  let page: LifeAsksPage;

  beforeEach(() => {
    page = new LifeAsksPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

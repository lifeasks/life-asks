import Answer from '../answer/model';

interface Question {
  id: number,
  text: string,
  featured: boolean,
  likes: number,
  visible: boolean,
  category?: any
  created_at?: Date,
  updated_at?: Date,
  answers: Array<Answer>
}

export default Question;
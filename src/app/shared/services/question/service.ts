import { Injectable } from '@angular/core';

import Question from './model';
import { HttpService } from "../http";

@Injectable()
export class QuestionService {
  private readonly PATH = '/questions';
  constructor(private httpService: HttpService) {}

  list(): Promise<Array<Question>> {
    return this.httpService.get(this.PATH);
  }
}
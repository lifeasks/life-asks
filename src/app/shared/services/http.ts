import { Injectable } from '@angular/core';
import { Response, Http, Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../../environments/environment';

@Injectable()
export class HttpService {
  constructor(private http: Http) {}

  get(path: String): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get(`${env.api}${path}`)
        .subscribe(data => {
          resolve(data.json());
        }, reject);
    });
  }

  post(path: String, body: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .post(`${env.api}${path}`, body, this.buildRequestOptionsforPost())
        .subscribe(data => {
          resolve(data.json());
        }, reject);
    });
  }

  private buildRequestOptionsforPost(): RequestOptions {
    const nHeaders = new Headers();
    nHeaders.append('Content-Type', 'application/vnd.api+json');
    nHeaders.append('Accept', 'application/vnd.api+json');
    return new RequestOptions({
      headers: nHeaders
    });
  }
}
interface Author {
  id: number,
  name: string,
  last_name: string,
  avatar_url?: string,
  fb_token?: string,
  created_at?: Date,
  updated_at?: Date
}

export default Author;
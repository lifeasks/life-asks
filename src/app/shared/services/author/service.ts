import { Injectable } from '@angular/core';

import Author from './model';
import { HttpService } from "../http";

@Injectable()
export class AuthorService {
  private readonly PATH = '/authors';
  constructor(private httpService: HttpService) {}

  save(author: Author): Promise<Author> {
    return this.httpService.post(this.PATH, author);
  }
}
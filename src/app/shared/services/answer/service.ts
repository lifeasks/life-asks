import { Injectable } from '@angular/core';

import Answer from './model';
import { HttpService } from "../http";

@Injectable()
export class AnswerService {
  private readonly PATH = '/answers';
  constructor(private httpService: HttpService) {}

  save(answer: Answer): Promise<Answer> {
    return this.httpService.post(this.PATH, answer);
  }

  like(id: Number): Promise<Answer> {
    return this.httpService.post(`${this.PATH}/${id}/like`, { id: id });
  }
}
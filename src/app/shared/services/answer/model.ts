import Author from '../author/model';

interface Answer {
  id?: number,
  text: string,
  extra_text?: string,
  likes: number,
  published: boolean,
  question_id: number,
  author_id?: number,
  created_at?: Date,
  updated_at?: Date
}

export default Answer;
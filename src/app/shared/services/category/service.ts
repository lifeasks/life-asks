import { Injectable } from '@angular/core';

import Category from './model';
import { HttpService } from "../http";

@Injectable()
export class CategoryService {
  private readonly PATH = '/categories';
  constructor(private httpService: HttpService) {}

  list(): Promise<Array<Category>> {
    return this.httpService.get(this.PATH);
  }
}
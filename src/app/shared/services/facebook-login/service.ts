import { Injectable, EventEmitter } from '@angular/core';

import Author from '../author/model';

import { FacebookService, LoginOptions, LoginResponse } from 'ngx-facebook';

export interface FacebookProfileData {
  name: String,
  last_name: String,
  avatar_url: String,
  fb_token: String
}

@Injectable()
export class FacebookLoginService {
  public loginChanged: EventEmitter<boolean> = new EventEmitter();

  constructor(private fbService: FacebookService) {}

  getUser(): Promise<any> {
    const options: LoginOptions = {
      scope: 'public_profile'
    };
    return new Promise((resolve, reject) => {
      this.fbService.getLoginStatus()
        .then(login => {
          if (login.status === 'connected') {
            this.fbService.api('/me', 'get', { fields: 'id, first_name,last_name,picture' })
              .then(apiResponse => {
                console.log('Authenticated at first');
                resolve(this.parseFacebookResponse(apiResponse));
              }).catch(reject);
          } else {
            this.fbService.login()
              .then((response: LoginResponse) => this.fbService.api('/me', 'get', { fields: 'id, first_name, last_name, picture' }))
              .then(apiResponse => {
                console.log('Authenticated after login');
                resolve(this.parseFacebookResponse(apiResponse));
              })
              .catch(reject);
          }
        }).catch(reject);
    });
  }

  getAuthorInSession(): Author {
    return JSON.parse(localStorage.getItem('la_author'));
  }

  inSession(): boolean {
    return !!localStorage.getItem('la_author');
  }

  login(session: string) {
    localStorage.setItem('la_author', session);
    this.loginChanged.emit(true);
  }

  logout() {
    localStorage.removeItem('la_author');
    this.loginChanged.emit(false);
  }

  private parseFacebookResponse(facebookResponse): FacebookProfileData {
    return {
      name: facebookResponse.first_name,
      last_name: facebookResponse.last_name,
      fb_token: facebookResponse.id,
      avatar_url: facebookResponse.picture.data.url || ''
    }
  }
}
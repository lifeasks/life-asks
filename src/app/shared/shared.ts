import { environment as env } from '../../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FacebookModule } from 'ngx-facebook';
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';
import { FacebookService, InitParams } from 'ngx-facebook';
import {Ng2PageScrollModule} from 'ng2-page-scroll';

import NavbarComponent from './components/navbar/navbar';
import FeaturedQuestionComponent from './components/featured-question/featured-question';
import QuestionComponent from './components/question/question';
import MenuComponent from './components/menu/menu';
import AdditionalQuestionComponent from './components/additional-question/additional-question';
import UnauthenticatedPostComponent from './components/unauthenticated-post/unauthenticated-post';
import SubmissionConfirmationComponent from './components/submission-confirmation/submission-confirmation';
import AnswerComponent from './components/answer/answer';

import { HttpService } from "./services/http";
import { QuestionService } from "./services/question/service";
import { AuthorService } from "./services/author/service";
import { AnswerService } from "./services/answer/service";
import { CategoryService } from "./services/category/service";
import { FacebookLoginService } from "./services/facebook-login/service";

const components: Array<any> = [
  NavbarComponent,
  MenuComponent,
  FeaturedQuestionComponent,
  QuestionComponent,
  AdditionalQuestionComponent,
  UnauthenticatedPostComponent,
  SubmissionConfirmationComponent,
  AnswerComponent
];

const providers: Array<any> = [
  FacebookService,
  HttpService,
  QuestionService,
  AuthorService,
  AnswerService,
  CategoryService,
  FacebookLoginService
];

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AngularFontAwesomeModule,
    FacebookModule,
    Ng2PageScrollModule
  ],
  declarations: components,
  providers: providers,
  entryComponents: [
    MenuComponent,
    UnauthenticatedPostComponent,
    SubmissionConfirmationComponent
  ],
  exports: components
})
export class SharedModule {
  constructor(fbService: FacebookService) {
    let initParams: InitParams = {
      appId: env.fbApiId,
      xfbml: true,
      version: 'v2.8'
    };
    fbService.init(initParams);
  }
}

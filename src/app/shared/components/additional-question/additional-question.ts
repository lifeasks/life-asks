import { Component, ViewEncapsulation, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'AdditionalQuestion',
  templateUrl: './additional-question.html',
  styleUrls: ['./additional-question.scss'],
  encapsulation: ViewEncapsulation.None
})
export default class AdditionalQuestionComponent {

  @Output() additionalTextChanged: EventEmitter<string> = new EventEmitter<string>();

  private selectedOption: string = '';
  private addtionalText: string = '';
  private options: Array<string> = [
    'It matters to me because',
    'It\'s awesome because',
    'What I would like instead is',
    'I love it because'
  ];
  private extraText: String;

  updateSelectedOption(selectedOption) {
    this.additionalTextChanged.emit(selectedOption.concat(' ').concat(this.addtionalText));
  }

  updateAdditionalText(text) {
    this.additionalTextChanged.emit(this.selectedOption.concat(' ').concat(text));
  }
}

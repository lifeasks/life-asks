import { Component, ViewEncapsulation, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'Menu',
  templateUrl: './menu.html',
  styleUrls: ['./menu.scss'],
  encapsulation: ViewEncapsulation.None
})
export default class MenuComponent {
  constructor(private activeModal: NgbActiveModal) {}
  close() {
    this.activeModal.dismiss();
  }
}

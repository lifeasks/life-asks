import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import Answer from '../../services/answer/model';
import { AnswerService } from '../../services/answer/service';

@Component({
  selector: 'Answer',
  templateUrl: './answer.html',
  styleUrls: ['./answer.scss'],
  encapsulation: ViewEncapsulation.None
})
export default class AnswerComponent {
  @Input() answer: Answer;
  private liked: boolean;

  constructor(private answerService: AnswerService) { }

  like() {
    this.answer.likes += 1;
    this.liked = true;
    this.answerService.like(this.answer.id)
      .catch(error => {
        console.log(error);
        this.answer.likes -= 1;
        this.liked = false;
      });
  }
}

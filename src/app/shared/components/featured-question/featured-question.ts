import { Component, Input, ViewEncapsulation, OnInit } from '@angular/core';
import UnauthenticatedPostComponent from "../unauthenticated-post/unauthenticated-post";
import SubmissionConfirmationComponent from '../submission-confirmation/submission-confirmation';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Question from '../../services/question/model';
import Answer from '../../services/answer/model';
import { AnswerService } from '../../services/answer/service';
import { FacebookLoginService } from '../../services/facebook-login/service';
import Author from '../../services/author/model';
import {PageScrollConfig} from 'ng2-page-scroll';
import {falseIfMissing} from "protractor/built/util";

@Component({
  selector: 'FeaturedQuestion',
  templateUrl: './featured-question.html',
  styleUrls: ['./featured-question.scss'],
  encapsulation: ViewEncapsulation.None
})
export default class FeaturedQuestionComponent implements OnInit {
  @Input() question: Question;
  private newAnswer: any = {};
  private visibleAnswers: Array<Answer>;

  private showingAdditionalQuestion: boolean = false;
  private showingAnswers: boolean = false;

  constructor(private modalService: NgbModal, private answerService: AnswerService, private fbService: FacebookLoginService) { }

  ngOnInit() {
    this.filterUnpublishedAnswers();
  }

  private filterUnpublishedAnswers() {
    this.visibleAnswers = this.question.answers.filter(answer => answer.published);
  }

  showAdditionalQuestion() {
    this.showingAdditionalQuestion = true;
  }
  showAnswers() {
    this.showingAdditionalQuestion = false;
    this.showingAnswers = true;
  }

  hideAnswers() {
    this.showingAnswers = false;
  }

  updateExtraText(extraText) {
    this.newAnswer.extra_text = extraText;
  }

  post() {
    if (this.fbService.inSession()) {
      const author = this.fbService.getAuthorInSession();
      this.postUsingExistingSession(author);
    } else {
      this.showConfirmPostModal()
    }
  }


  private postUsingExistingSession(author) {
    this.newAnswer.author_id = author.id;
    this.newAnswer.published = true;
    this.newAnswer.question_id = this.question.id;
    this.answerService.save(this.newAnswer)
      .then(resultAnswer => {
        this.modalService.open(SubmissionConfirmationComponent, {windowClass: 'submission-confirmation-modal'});
        this.newAnswer.text = '';
      })
      .catch(console.error);
  }

  private showConfirmPostModal() {
    this.showingAdditionalQuestion = false;
    const modalRef = this.modalService.open(UnauthenticatedPostComponent, { windowClass: 'unauth-modal' });
    modalRef.componentInstance.answer = {
      text: this.newAnswer.text,
      extra_text: this.newAnswer.extra_text,
      question_id: this.question.id,
      published: false,
      author_id: null
    };
    this.newAnswer.text = '';
  }

  cancel() {
    this.showingAdditionalQuestion = false;
  }
}

import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { FacebookLoginService } from '../../services/facebook-login/service';

import MenuComponent from '../menu/menu';

@Component({
  selector: 'Navbar',
  templateUrl: './navbar.html',
  styleUrls: ['./navbar.scss'],
  encapsulation: ViewEncapsulation.None
})
export default class NavbarComponent {
  private isLoggedIn: boolean;

  constructor(private modalService: NgbModal, private fbService: FacebookLoginService) {
    this.isLoggedIn = this.fbService.inSession();
    this.fbService.loginChanged.subscribe(loggedIn => {
      this.isLoggedIn = loggedIn;
    });
  }

  signOut() {
    this.fbService.logout();
  }

  showMenu() {
    this.modalService.open(MenuComponent);
  }
}

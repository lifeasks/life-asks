import { Component, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import SubmissionConfirmationComponent from "../submission-confirmation/submission-confirmation";
import Author from '../../services/author/model';
import { AuthorService } from '../../services/author/service';
import Answer from '../../services/answer/model';
import { AnswerService } from '../../services/answer/service';
import { FacebookLoginService } from "../../services/facebook-login/service";

@Component({
  selector: 'UnauthenticatedPost',
  templateUrl: './unauthenticated-post.html',
  styleUrls: ['./unauthenticated-post.scss'],
  encapsulation: ViewEncapsulation.None
})
export default class UnauthenticatedPostComponent {
  @Input() answer: Answer;
  @Output() answerAdded: EventEmitter<Answer> = new EventEmitter();
  @Output() loginSucceeded: EventEmitter<any> = new EventEmitter();

  constructor(
    private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private authorService: AuthorService,
    private answerService: AnswerService,
    private fbService: FacebookLoginService
  ) {
  }

  postUsingFacebook() {
    this.activeModal.dismiss();
    this.fbService.getUser()
      .then(authorFbData => {
        this.authorService.save(authorFbData)
          .then(author => {
            this.answer.author_id = author.id;
            this.answer.published = true;
            this.fbService.login(JSON.stringify(author));
            this.saveAnswer(this.answer);
          }).catch(console.error);
      })
      .catch(console.error);
  }

  postAnonymously() {
    this.saveAnswer(this.answer);
  }

  private saveAnswer(answer: Answer) {
    this.answerService.save(answer)
      .then(resultAnswer => {
        this.answerAdded.emit(resultAnswer);
        this.activeModal.dismiss();
        this.modalService.open(SubmissionConfirmationComponent, {windowClass: 'submission-confirmation-modal'});
      })
      .catch(console.error);
  } 
}

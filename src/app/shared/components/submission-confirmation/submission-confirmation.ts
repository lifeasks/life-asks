import { Component, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'SubmissionConfirmation',
  templateUrl: './submission-confirmation.html',
  styleUrls: ['./submission-confirmation.scss'],
  encapsulation: ViewEncapsulation.None
})
export default class SubmissionConfirmationComponent {
  constructor(private activeModal: NgbActiveModal) {}

  return() {
    this.activeModal.dismiss();
  }
}

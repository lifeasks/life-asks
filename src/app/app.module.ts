import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';

import { SharedModule } from './shared/shared';
import {Ng2PageScrollModule} from 'ng2-page-scroll';


import AppComponent from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    NgbModule.forRoot(),
    AngularFontAwesomeModule,
    BrowserModule,
    SharedModule,
    Ng2PageScrollModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, ViewEncapsulation } from '@angular/core';
import Question from "./shared/services/question/model";
import Category from "./shared/services/category/model";
import { QuestionService } from "./shared/services/question/service";
import { CategoryService } from "./shared/services/category/service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export default class AppComponent {
  private questions: Array<Question> = [];
  private featuredQuestions: Array<Question> = [];
  private selectedCategory: Category;
  private categories: Array<Category> = [];
  private unfilteredQuestions: Array<Question>;

  constructor(private questionService: QuestionService, private categoryService: CategoryService) {
    questionService.list()
      .then(this.classifyQuestions.bind(this));
    categoryService.list()
      .then(categories => {
        this.categories = categories;
      });
  }

  filterByCategory(category?: Category) {
    this.selectedCategory = category;
    this.questions = this.unfilteredQuestions;
    this.questions = this.questions.filter(question => {
      if (!category) {
        return true;
      }
      return question.category ? question.category.name === category.name : false;
    });
  }

  /**
   * Classifies questions between featured and non-featured
   * @param questions fetched from server
   */
  private classifyQuestions(questions) {
    this.featuredQuestions = questions.filter((question) => question.featured && question.visible);
    this.questions = questions.filter((question) => !question.featured && question.visible);
    this.unfilteredQuestions = this.questions;
  }
}
